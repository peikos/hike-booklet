doc:
	@clear
	@echo "Create a file target.pages containing a space-separated list of pages (e.g. \"0b 1a 1b 2a 2b 3a 3c 0a\") and make sure the associated source files 0.svg, 1.svg, 2.svg and 3.svg exist."
	@echo "Then run make again with the target PDF name (e.g. \"make verkenners.pdf\")."
	@echo "This will generate verkenners.pdf and verkenners-book.pdf."

help: doc

.SECONDARY:

%.pdf: %.pages.pdf
	pdfbook2 -o0 -i0 -t0 -b0 $^
	mv "$(basename $^)-book.pdf" "$@"

%.pages.pdf: %.pages
	for target in $(addsuffix .temp.pdf, $(shell cat $^)); do \
		$(MAKE) $$target ; \
	done
	pdftk $(addsuffix .temp.pdf, $(shell cat $^)) cat output $@

%a.temp.pdf %b.temp.pdf: %.temp.pdf
	mutool poster -x 2 $^ $(addsuffix s.temp.pdf, $(basename $(basename $^)))
	pdftk $(addsuffix s.temp.pdf, $(basename $(basename $^))) cat 1 output $(addsuffix a.temp.pdf, $(basename $(basename $^)))
	pdftk $(addsuffix s.temp.pdf, $(basename $(basename $^))) cat 2 output $(addsuffix b.temp.pdf, $(basename $(basename $^)))

%.temp.pdf: %.svg
	inkscape $^ -d 300 --export-pdf=$(addsuffix .temp.pdf, $^)
	gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS="/ebook" -sOutputFile=$@ $(addsuffix .temp.pdf, $^)

clean:
	-@rm *.pdf

clean-intermediate:
	-@rm *.temp.pdf *.pages.pdf

remake: clean all

tools: /usr/bin/yay
	yay -S --needed --norebuild mupdf-tools inkscape texlive-core pdftk ghostscript
